import json
import os
from datetime import datetime

import pytest

from src.aliases import Quotes
from src.dump_utils import DumpController

DumpController.DUMP_PATH = './tmp'


def prelude():
    os.makedirs(DumpController.DUMP_PATH, exist_ok=True)


def cleanup():
    files: list[str] = os.listdir(DumpController.DUMP_PATH)
    for file_name in files:
        os.remove(
            os.path.join(DumpController.DUMP_PATH, file_name)
        )
    os.rmdir(DumpController.DUMP_PATH)


@pytest.mark.asyncio
async def test_make_dump():
    DumpController.make_dump()
    assert os.path.exists(DumpController.DUMP_PATH)
    assert len(os.listdir(DumpController.DUMP_PATH)) != 0
    cleanup()


@pytest.mark.asyncio
async def test_get_latest_path():
    prelude()
    latest: str = DumpController.get_latest_path()
    assert latest is None

    DumpController.make_dump()
    latest: str = DumpController.get_latest_path()
    assert type(latest) is str
    cleanup()


@pytest.mark.asyncio
async def test_get_latest_dump():
    prelude()
    latest: Quotes = DumpController.get_latest_dump()
    assert latest is None

    DumpController.make_dump()
    latest: Quotes = DumpController.get_latest_dump()
    assert latest is not None
    assert type(latest) is type(Quotes())
    latest_path: str = os.path.join(
        DumpController.DUMP_PATH,
        os.listdir(DumpController.DUMP_PATH)[0]
    )
    with open(latest_path, 'r') as file:
        assert latest == json.load(file)
    cleanup()


@pytest.mark.asyncio
async def test_generate_dump_name():
    time: datetime = datetime.now()
    name: str = DumpController.generate_dump_name(time)
    assert name is not None
    assert time.strftime(DumpController.dump_format()) == name
