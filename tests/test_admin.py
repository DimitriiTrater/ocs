import pytest
from starlette.testclient import TestClient
import src.admin as admin

CLIENT: TestClient = TestClient(admin.router)


def test_admin():
    response = CLIENT.get('/admin')
    assert response.status_code == 200
    assert response.template.name == 'admin.html'
    assert 'request' in response.context


def test_admin_import():
    response = CLIENT.get('/admin/import')
    assert response.status_code == 200
    assert response.template.name == 'import.html'
    assert 'request' in response.context


def test_admin_edit():
    response = CLIENT.get('/admin/edit')
    assert response.status_code == 200
    assert response.template.name == 'edit.html'
    assert 'request' in response.context


@pytest.mark.asyncio
async def test_edit_submit():
    await admin.edit_submit('EUR', 1)


def test_admin_delete():
    response = CLIENT.get('/admin/delete')
    assert response.status_code == 200
    assert response.template.name == 'delete.html'
    assert 'request' in response.context


@pytest.mark.asyncio
async def test_delete_submit():
    await admin.delete_submit('EUR')


@pytest.mark.asyncio
async def test_admin_show():
    await admin.show()
