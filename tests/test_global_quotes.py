from src.aliases import Quotes
from src.global_quotes import GlobalQuotes

import pytest

TEST_CURRENCY_NAME: str = 'CUR'
TEST_CURRENCY_VALUE: float = 12.34


@pytest.mark.asyncio
async def test_global_quotes_type():
    assert type(GlobalQuotes().get_quotes()) is type(Quotes())


@pytest.mark.asyncio
async def test_global_quotes_import_request():
    assert GlobalQuotes.import_request()


@pytest.mark.asyncio
async def test_global_quotes_update():
    GlobalQuotes.update_quotes(TEST_CURRENCY_NAME, TEST_CURRENCY_VALUE)
    assert GlobalQuotes.get_quotes()[TEST_CURRENCY_NAME] == TEST_CURRENCY_VALUE


@pytest.mark.asyncio
async def test_global_quotes_delete():
    GlobalQuotes.update_quotes(TEST_CURRENCY_NAME, TEST_CURRENCY_VALUE)
    GlobalQuotes.delete_quote(TEST_CURRENCY_NAME)
    assert TEST_CURRENCY_NAME not in GlobalQuotes.get_quotes()


if __name__ == '__main__':
    test_global_quotes_type()
    test_global_quotes_import_request()
    test_global_quotes_update()
    test_global_quotes_delete()
