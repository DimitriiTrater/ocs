import pytest

from src.converter import Converter

TEST_DICT = {'EUR': 1, 'USD': 1.0926, 'JPY': 160.43}
AMOUNT = 100

converter = Converter(TEST_DICT)


@pytest.mark.asyncio
async def test_direct_convert():
    assert (await converter.convert('EUR', 'USD', AMOUNT) ==
            AMOUNT * TEST_DICT['USD'])


@pytest.mark.asyncio
async def test_indirect_to_euro_convert():
    assert (await converter.convert('JPY', 'EUR', AMOUNT) ==
            AMOUNT / TEST_DICT['JPY'] * TEST_DICT['EUR'])


@pytest.mark.asyncio
async def test_indirect_convert():
    assert (await converter.convert('USD', 'JPY', AMOUNT) ==
            AMOUNT / TEST_DICT['USD'] * TEST_DICT['JPY'])


@pytest.mark.asyncio
async def test_singleton():
    c1 = Converter({'EUR': 1, 'USD': 3})
    c2 = Converter({'EUR': 1, 'USD': 5})
    assert (c1 is c2) and (await c1.convert('EUR', 'USD', 5)
                           == await c2.convert('EUR', 'USD', 5))


if __name__ == '__main__':
    test_direct_convert()
    test_indirect_to_euro_convert()
    test_indirect_convert()
    test_singleton()
