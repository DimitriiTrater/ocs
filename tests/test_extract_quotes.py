from src.extract_quotes import EuroQuotesDaily
from xml.etree.ElementTree import fromstring
import os.path

TEST_XML = '''<?xml version="1.0" encoding="UTF-8"?>
<gesmes:Envelope xmlns:gesmes="http://www.gesmes.org/xml/2002-08-01"
xmlns="http://www.ecb.int/vocabulary/2002-08-01/eurofxref">
    <gesmes:subject>Reference rates</gesmes:subject>
    <gesmes:Sender>
        <gesmes:name>European Central Bank</gesmes:name>
    </gesmes:Sender>
    <Cube>
        <Cube time='2024-03-11'>
            <Cube currency='USD' rate='1.0926'/>
            <Cube currency='JPY' rate='160.43'/>
        </Cube>
    </Cube>
</gesmes:Envelope>'''

TEST_DICT = {'EUR': 1, 'USD': 1.0926, 'JPY': 160.43}

TEST_JSON_NAME = 'test.json'


def test_get_response_as_text():
    assert EuroQuotesDaily().get_response().status_code == 200


def test_convert_response_to_text():
    assert EuroQuotesDaily().convert_response_to_text() != ''


def test_convert_xml_tree_to_dict():
    assert EuroQuotesDaily().convert_xml_tree_to_dict(
        EuroQuotesDaily.convert_text_to_xml_tree()) != {}


def test_create_json():
    EuroQuotesDaily.create_json(TEST_JSON_NAME)
    assert os.path.isfile(TEST_JSON_NAME) is True
    os.remove(TEST_JSON_NAME)


def test_conversion_from_xml_to_dict_check():
    assert (EuroQuotesDaily.convert_xml_tree_to_dict(fromstring(TEST_XML))
            == TEST_DICT)


if __name__ == '__main__':
    test_get_response_as_text()
    test_convert_response_to_text()
    test_convert_xml_tree_to_dict()
    test_create_json()
    test_conversion_from_xml_to_dict_check()
