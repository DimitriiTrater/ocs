from starlette.staticfiles import StaticFiles
from starlette.testclient import TestClient
import src.converter_route as converter_route
import src.admin as admin

converter_route.router.mount(
    "/static",
    StaticFiles(directory="static"),
    name="static",
)

CLIENT_ROUTE: TestClient = TestClient(converter_route.router)
CLIENT_ADMIN: TestClient = TestClient(admin.router)


def test_converter_page():
    CLIENT_ADMIN.get('/admin/import')
    response = CLIENT_ROUTE.get('/converter')
    assert response.status_code == 200
    assert response.template.name == 'converter.html'
    assert 'request' in response.context


def test_converter_submit():
    response = CLIENT_ROUTE.post('/converter/submit')
    assert response.status_code == 200
    assert response.template.name == 'converter.html'
    assert 'request' in response.context
