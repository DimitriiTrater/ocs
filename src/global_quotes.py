from datetime import datetime, timedelta

from .aliases import Quotes
from .exceptions import FailedImport
from .extract_quotes import EuroQuotesDaily
from .dump_utils import DumpController


class GlobalQuotes:
    __GLOBAL_QUOTES: Quotes = {}
    __REFRESH_RATE: timedelta = timedelta(days=1)

    @classmethod
    def get_quotes(cls) -> Quotes:
        if DumpController.get_latest_dump() is None:
            DumpController.make_dump()

        dump_stamp: datetime = DumpController.parse_dump_name(
            DumpController.get_latest_path()
        )
        if (datetime.now() - dump_stamp) > cls.__REFRESH_RATE:
            DumpController.make_dump()
            latest: Quotes = DumpController.get_latest_dump()
            if not latest:
                raise FailedImport
            cls.__GLOBAL_QUOTES = latest

        if not cls.__GLOBAL_QUOTES:
            latest: Quotes = DumpController.get_latest_dump()
            if not latest:
                raise FailedImport
            cls.__GLOBAL_QUOTES = latest

        return cls.__GLOBAL_QUOTES

    @classmethod
    def import_request(cls) -> bool:
        imported_quotes: Quotes = (
            EuroQuotesDaily.convert_xml_tree_to_dict(
                EuroQuotesDaily.convert_text_to_xml_tree()))
        for key, value in imported_quotes.items():
            cls.__GLOBAL_QUOTES[key] = value
        if cls.__GLOBAL_QUOTES:
            return True
        return False

    @classmethod
    def update_quotes(cls, name: str, value: float) -> None:
        cls.__GLOBAL_QUOTES[name] = value

    @classmethod
    def delete_quote(cls, name: str) -> None:
        del cls.__GLOBAL_QUOTES[name]
