from fastapi import APIRouter, Form, Request
from .global_quotes import GlobalQuotes
from .templates import templates
from .aliases import TemplateResponse, Quotes
from .converter import Converter
from .exceptions import CurrencyEmpty
from typing import Any

router = APIRouter()


@router.get("/converter")
async def converter_page(request: Request) -> TemplateResponse:
    quotes: Quotes = GlobalQuotes.get_quotes()

    if not list(quotes.keys()):
        raise CurrencyEmpty

    first_currency: str = list(quotes.keys())[0]
    context: dict[str, Any] = {
        'request': request,
        'currencies': quotes.keys(),
        'base_currency': first_currency,
        'target_currency': first_currency,
        'title': 'Open Currency Swap'
    }
    return templates.TemplateResponse('converter.html', context=context)


@router.post("/converter/submit")
async def converter_submit(
        request: Request,
        base_value: float | str = Form(0),
        base_currency: str = Form('EUR'),
        target_currency: str = Form('EUR')
):
    quotes: Quotes = GlobalQuotes.get_quotes()
    try:
        value: float = await Converter(quotes).convert(
            base_currency,
            target_currency,
            float(base_value)
        )
        context: dict[str, Any] = {
            'request': request,
            'currencies': quotes,
            'base_value': base_value,
            'base_currency': base_currency,
            'target_value': value,
            'target_currency': target_currency,
            'error': False,
            'title': 'Open Currency Swap'
        }
        return templates.TemplateResponse('converter.html', context=context)
    except (ValueError, KeyError):
        context: dict[str, Any] = {
            'request': request,
            'currencies': quotes,
            'base_value': base_value,
            'base_currency': base_currency,
            'target_currency': target_currency,
            'error': True,
            'title': 'Open Currency Swap'
        }
        return templates.TemplateResponse('converter.html', context=context)
