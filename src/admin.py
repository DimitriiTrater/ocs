from fastapi import APIRouter, Form, Request
from fastapi.responses import HTMLResponse
from .global_quotes import GlobalQuotes
from .templates import templates
from .aliases import Quotes, TemplateResponse

router = APIRouter()


@router.get("/admin", response_class=HTMLResponse)
async def root(request: Request) -> TemplateResponse:
    links = [
        {'name': 'show', 'link': 'admin/show'},
        {'name': 'import', 'link': 'admin/import'},
        {'name': 'edit', 'link': 'admin/edit'},
        {'name': 'delete', 'link': 'admin/delete'},
    ]
    context = {
        'request': request,
        'links': links,
        'title': 'admin'
    }
    return templates.TemplateResponse('admin.html', context=context)


@router.get('/admin/import', response_class=HTMLResponse)
async def import_quotes(request: Request) -> TemplateResponse:
    if GlobalQuotes.import_request():
        return templates.TemplateResponse(
            'import.html',
            context={
                'request': request,
                'quotes': GlobalQuotes.get_quotes(),
                'title': 'import'
            })
    return templates.TemplateResponse(
        'import.html',
        {'request': request, 'title': 'bad_import'}
    )


@router.get('/admin/edit', response_class=HTMLResponse)
async def edit(request: Request) -> TemplateResponse:
    context = {
        'request': request,
        'title': 'edit'
    }
    return templates.TemplateResponse('edit.html', context=context)


@router.post('/admin/edit/submit')
async def edit_submit(name=Form(), value=Form()) -> Quotes | dict[str, str]:
    try:
        float(value)
    except ValueError:
        return {'error': 'Invalid value'}
    GlobalQuotes.update_quotes(name, float(value))
    return GlobalQuotes.get_quotes()


@router.get('/admin/delete', response_class=HTMLResponse)
async def delete(request: Request) -> TemplateResponse:
    context = {
        'request': request,
        'title': 'delete',
    }
    return templates.TemplateResponse('delete.html', context=context)


@router.post('/admin/delete/submit')
async def delete_submit(name=Form()) -> tuple[Quotes, dict[str, str]]:
    deleted_quote_value: float = GlobalQuotes.get_quotes()[name]
    GlobalQuotes.delete_quote(name)
    return (GlobalQuotes.get_quotes(),
            {'deleted_currency': f'{name}: {deleted_quote_value}'})


@router.get('/admin/show')
async def show() -> Quotes:
    return GlobalQuotes.get_quotes()
