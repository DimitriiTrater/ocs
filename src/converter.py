from typing import Dict


class Converter:
    def __new__(cls, quotes: Dict[str, float]):
        cls.__quotes = quotes
        if not hasattr(cls, 'instance'):
            cls.instance = super(Converter, cls).__new__(cls)
        return cls.instance

    async def convert(self, c_from: str, c_to: str, amount: float) -> float:
        default_currency = list(self.__quotes.values()).index(1)
        if c_from != default_currency:
            return amount / self.__quotes[c_from] * self.__quotes[c_to]
        return amount * self.__quotes[c_to]
