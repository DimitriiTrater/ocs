from datetime import datetime
from os import PathLike
import json
import os
import glob

from .aliases import Quotes
from .extract_quotes import EuroQuotesDaily


class DumpController:
    DUMP_PATH: PathLike = './dumps/'
    __DUMP_FORMAT: str = '%d_%m_%y.json'

    @classmethod
    def dump_format(cls):
        return cls.__DUMP_FORMAT

    @classmethod
    def generate_dump_name(cls, time: datetime = datetime.now()) -> str:
        return time.strftime(format=cls.__DUMP_FORMAT)

    @classmethod
    def parse_dump_name(cls, dump_name: str) -> datetime:
        return datetime.strptime(
            os.path.basename(dump_name),
            cls.__DUMP_FORMAT
        )

    @classmethod
    def get_latest_path(cls) -> str | None:
        file_paths: list[str] = (
            glob.glob(os.path.join(DumpController.DUMP_PATH, '*.json'))
        )
        if not file_paths:
            return None

        return max(
            file_paths,
            key=lambda path:
            DumpController.parse_dump_name(os.path.basename(path))
        )

    @classmethod
    def get_latest_dump(cls) -> Quotes | None:
        latest: str = DumpController.get_latest_path()
        if not latest:
            return None

        if directory := os.path.dirname(latest):
            os.makedirs(
                directory,
                exist_ok=True)

        quotes: Quotes = {}
        with open(latest, 'r') as file:
            quotes = json.load(file)
        return quotes

    @classmethod
    def make_dump(cls) -> None:
        EuroQuotesDaily.create_json(
            os.path.join(
                DumpController.DUMP_PATH,
                DumpController.generate_dump_name()))
