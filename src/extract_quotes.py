import os
from json import dump
from os import PathLike
from xml.etree.ElementTree import fromstring, Element
from typing import Dict, LiteralString
from requests import Response
import requests


class EuroQuotesDaily:
    URL: LiteralString = \
        'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml'

    DEFAULT_JSON_NAME: PathLike[str] = 'euro_quotes.json'

    @classmethod
    def get_response(cls) -> Response:
        return requests.get(cls.URL)

    @classmethod
    def convert_response_to_text(cls) -> str:
        return cls.get_response().text

    @classmethod
    def convert_text_to_xml_tree(cls) -> Element:
        return fromstring(cls.convert_response_to_text())

    @classmethod
    def convert_xml_tree_to_dict(cls, root: Element) -> Dict[str, float]:
        euro_quotes: Dict[str, float] = {'EUR': 1}
        quotes: Element = root[2][0]
        for quote in quotes:
            euro_quotes[quote.attrib['currency']] = float(quote.attrib['rate'])
        return euro_quotes

    @classmethod
    def create_json(
            cls,
            path: PathLike[str] | str = DEFAULT_JSON_NAME
    ) -> None:
        euro_quotes = EuroQuotesDaily.convert_xml_tree_to_dict(
            EuroQuotesDaily.convert_text_to_xml_tree())
        if directory := os.path.dirname(path):
            os.makedirs(
                directory,
                exist_ok=True)
        with open(path, 'w') as f:
            dump(euro_quotes, f)


if __name__ == '__main__':
    EuroQuotesDaily.create_json()
