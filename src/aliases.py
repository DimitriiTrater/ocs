from starlette.templating import _TemplateResponse
from typing import TypeAlias

TemplateResponse: TypeAlias = _TemplateResponse
Quotes: TypeAlias = dict[str, float]
