from fastapi import FastAPI, Request
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles
import src.admin as admin
import src.converter_route as converter_route

app = FastAPI()

app.mount(
    "/static",
    StaticFiles(directory="static"),
    name="static",
)

app.include_router(admin.router)
app.include_router(converter_route.router)


@app.get("/")
async def converter_redirect(request: Request):
    return RedirectResponse(request.url_for("converter_page"))
