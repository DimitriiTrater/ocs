# OCS - Open Currency Swap

## Description
We are just a free and open-source currency converter.

## Start up
First, just clone it.
```
git clone https://gitlab.com/DimitriiTrater/ocs.git
```
Start up it with docker (you need to be in root).
```
docker build -t ocs_image .
docker run -d --name ocs_container -p 80:80 ocs_image
```
Go to
```
http://localhost/
```
If all good you will see this image

![alt text](image.png)

